#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <asm/termbits.h>

int ttyin;
void cat1(FILE *f) {
  int c;

  setvbuf(f, (char *) 0, _IONBF, 0);
  while ((c = fgetc(f)) != EOF) {
    if (c == '\n' && f == stdin) {
      c = '\r';
      write(ttyin, &c, 1);
      c = '\n';
    }
    write(ttyin, &c, 1);
    fsync(ttyin);
  }
}

int main(int argc, char *argv[]) {
  int argi;
  FILE *f;

  setvbuf(stdin, (char *) 0, _IONBF, 0);
  ttyin = open("/tmp/ttye", O_WRONLY, O_CREAT);
  if (argc == 1) {
    {
      struct termios raw;
      ioctl(0, TCGETS, &raw);
      raw.c_lflag &= ~(ICANON);
      ioctl(0, TCSETSF, &raw);
    }

    cat1(stdin);

    {
      struct termios raw;
      ioctl(0, TCGETS, &raw);
      raw.c_lflag |= ICANON;
      ioctl(0, TCSETSF, &raw);
    }
  } else for (int argi = 1; argi < argc; ++argi) {
    f = fopen(argv[argi], "rb");
    cat1(f);
    fclose(f);
  }
  close(ttyin);
  return 0;
}
