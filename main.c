#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <raylib.h>

enum {
  NUL, SOH, STX, ETX, EOT, ENQ, ACK, BEL,
  BS, HT, LF, VT, FF, CR, SO, SI, DLE,
  XON, TAPE, XOFF, NTAPE, NAK, SYN, ETB,
  CAN, EM, SUB, ESC, FS, GS, RS, US,
  SP,

  DEL = 0x7f
};

static char *buffer;
size_t lines = 0, cursor = 0;
int state = STX;
int ttyin;
Sound beep;

int main(void) {
  	int display = GetCurrentMonitor();
    float scroll = 0;
    Font f;

    lines = 58;
    buffer = malloc(3655);
    memset(buffer, ' ', 3654);
    buffer[3654] = '\0';
    InitWindow(402, 696, "ttye");
    InitAudioDevice();
    if (IsWindowFullscreen())
      ToggleFullscreen();
    SetTargetFPS(60);
    f = LoadFont("./CourierPrime-Regular.ttf");
    mkfifo("/tmp/ttye", 0777);
    BeginDrawing();
    ClearBackground(RAYWHITE);
    DrawTextEx(f, "Pipe some data to `/tmp/ttye`.", (Vector2) { 13, 20 }, 24, 0, BLACK);
    EndDrawing();
    fprintf(stderr, "INFO: opening /tmp/ttye...\n", ttyin);
    if ((ttyin = open("/tmp/ttye", O_RDWR, O_NONBLOCK)) == -1)
      return -1;
    fprintf(stderr, "INFO: /tmp/ttye opened (%d)\n", ttyin);
    beep = LoadSound("beep.wav");

    while (!WindowShouldClose()) {
        BeginDrawing();
        ClearBackground(RAYWHITE);
        scroll -= GetMouseWheelMove() * 12;
        if (IsKeyPressed(KEY_UP) || IsKeyPressedRepeat(KEY_UP))
          scroll -= 12;
        if (IsKeyPressed(KEY_DOWN) || IsKeyPressedRepeat(KEY_DOWN))
          scroll += 12;
        if (IsKeyPressed(KEY_PAGE_UP) || IsKeyPressedRepeat(KEY_PAGE_UP))
          scroll -= 696;
        if (IsKeyPressed(KEY_PAGE_DOWN) || IsKeyPressedRepeat(KEY_PAGE_DOWN))
          scroll += 696;

        for (int i = 0; i < lines; ++i) {
          char c;

          c = buffer[(i+1) * 63];
          buffer[(i+1) * 63] = '\0';
          DrawTextEx(f, &buffer[i * 63], (Vector2) {0, i * 12 - scroll}, 12, 0, BLACK);
          buffer[(i+1) * 63] = c;
        }

        {
          char c;

          if (read(ttyin, &c, 1) == 1) {
            if (state == STX) {
              if (c == BS) {
                --cursor;
              } else if (c == CR) {
                cursor -= cursor % 63;
              } else if (c == LF) {
                cursor += 63;
              } else if (c == FF) {
                cursor += 3653;
                cursor -= cursor % 3654;
              } else if (c == HT) {
                cursor += 8;
                cursor -= (cursor % 63) % 8;
              } else if (c == VT) {
                cursor += 504;
                cursor -= cursor % 504;
              } else if (c == BEL) {
                PlaySound(beep);
              } else if (c == SOH || c == ETX || c == EOT) {
                state = c;
              } else if (c >= SP && c < DEL) {
                buffer[cursor++] = c;
              }
            } else if (state == SOH) {
              if (c == STX)
                state = c;
            } else if (state == ETX) {
              if (c == SOH || c == STX)
                state = c;
            }
            while (cursor > lines * 63) {
              buffer = realloc(buffer, lines * 63 + 64);
              memset(&buffer[lines * 63], ' ', 63);
              ++lines;
              buffer[lines * 63] = '\0';
            }
          }
        }
        EndDrawing();
    }
    UnloadFont(f);
    UnloadSound(beep);
    CloseWindow();
    free(buffer);
    close(ttyin);
    CloseAudioDevice();
    return 0;
}

