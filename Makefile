CFLAGS=-ldl -lpthread -lm ~/raylib/build/raylib/libraylib.a

main : main.c
	$(CC) -o $@ $^ $(CFLAGS)

all : main
